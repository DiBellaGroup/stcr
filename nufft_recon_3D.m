function imageOutput = nufft_recon_3D(KSpaceData, densityCompensation, Weights)
  % Reconstruct multi-coil non-Cartesian data

  % Use mask to determine dimensions
  dimensions = [KSpaceData.cartesianSize(1), KSpaceData.cartesianSize(2)];
  nCoil = KSpaceData.cartesianSize(3);

  % pre-allocation
  imageOutput = zeros(KSpaceData.cartesianSize);

  % obtain nufftObj
  nufftObj = FftTools.MultiNufft(KSpaceData.trajectory, densityCompensation, [0,0], dimensions);

  % Coil by coil recon
  for iCoil = 1:nCoil
    coilKSpace = KSpaceData.kSpace(:,:,iCoil);
    imageOutput(:,:,iCoil) = Stcr.reconstruct(coilKSpace, nufftObj, Weights);
  end

  imageOutput = combine_3D(imageOutput);
end
