function imageOutput = fft_recon_3D(KSpaceData, Weights)
  % Reconstruct multi-coil Cartesian data

  % Assume coils at end of dataset
  nCoils = KSpaceData.cartesianSize(3);
  imageOutput = zeros(KSpaceData.cartesianSize);

  % Coil by coil reconstruct
  for iCoil = 1:nCoils
    kMaskCoil = KSpaceData.cartesianMask(:,:,iCoil);
    kSpaceCoil = KSpaceData.cartesianKSpace(:,:,iCoil);
    % Create FFT Object
    fftObj = FftTools.MaskFft(kMaskCoil);
    % Coil by coil reconstruct
    imageOutput(:,:,iCoil) = Stcr.reconstruct(kSpaceCoil, fftObj, Weights);
  end

  % Now combine via sum of squares for display
  imageOutput = combine_3D(imageOutput);
end
