function imageOutput = fft_recon_4D(KSpaceData, Weights)
  % Reconstruct multi-coil and multi-time-frame Cartesian data

  % Assume coils at end of dataset
  nCoils = KSpaceData.cartesianSize(4);
  imageOutput = zeros(KSpaceData.cartesianSize);

  % Coil by coil reconstruct
  for iCoil = 1:nCoils
    kMaskCoil = KSpaceData.cartesianMask(:,:,:,iCoil);
    kSpaceCoil = KSpaceData.cartesianKSpace(:,:,:,iCoil);
    % Create FFT Object
    fftObj = FftTools.MaskFft(kMaskCoil);
    % Coil by coil reconstruct
    imageOutput(:,:,:,iCoil) = Stcr.reconstruct(kSpaceCoil, fftObj, Weights);
  end

  % Now combine via sum of squares for display
  imageOutput = combine_4D(imageOutput);
end
