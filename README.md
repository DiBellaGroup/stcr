# The Stcr Package

## Introduction

Stcr is an iterative Compressed Sensing image reconstruction method that was first published by [Adluru et. al in 2007](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=4193234&tag=1).

This package contains several key components which are explained in the following sections.

### Status

The Stcr package has been replaced by the Critter package. Please use that instead.

### Version

The VERSION.mat file contains a single variable `VERSION` which contains a string with the version number of this package. No modifications to this file should be made without updating this number according to [semantic versioning best practices](http://semver.org/).

### Package Management

Running `Stcr.verify` will examine the environment that Stcr sees and ensure that any packages it depends on are available and of the correct version.

### Tests

To make sure this software works as expected in the present environment, tests have been provided and can be run by executing `Stcr.test`.

### Docs

In the `docs/` directory some markdown files are provided explaining how to use each of the functions and classes this package provides.
