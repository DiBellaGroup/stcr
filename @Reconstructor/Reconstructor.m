classdef Reconstructor < handle
  properties (Constant)
    DEBUG = false;
    BETA_SQUARED = 1e-8;
    STEP_SIZE = 0.5;
    N_ITERATIONS = 70;
    MAGIC_SCALE_NUMBER = 4;
  end
  properties
    kSpaceInput
    fftObject
    Weights
    imageInput
    is3D

    scaleFactor
    imageEstimate
    maskedImageEstimate
    fidelityUpdateTerm
    temporalUpdateTerm
    spatialUpdateTerm
  end
  methods
    function self = Reconstructor(kSpaceInput, fftObject, Weights)
      self.kSpaceInput = single(kSpaceInput);
      self.fftObject = fftObject;
      self.Weights = Weights;
      self.set_3D_flag()
    end

    function set_3D_flag(self)
      if ndims(self.kSpaceInput) > 2
        self.is3D = true;
      else
        self.is3D = false;
      end
    end

    function finalImage = reconstruct(self)
      self.create_image_input()
      self.scale_image_input()
      self.iteratively_reconstruct()
      finalImage = self.unscale_image();
    end

    function create_image_input(self)
      self.imageInput = self.fftObject' * self.kSpaceInput;
    end

    function scale_image_input(self)
      maxIntensity = max(abs(self.imageInput(:)));
      self.scaleFactor = self.MAGIC_SCALE_NUMBER / maxIntensity;
      self.imageInput = single(self.imageInput * self.scaleFactor);
    end

    function imageEstimate = iteratively_reconstruct(self)
      self.pre_allocate_loop_variables();
      for iIteration = 1:self.N_ITERATIONS
        self.update_fidelity_term();
        if self.is3D
          self.update_temporal_term();
        end
        self.update_spatial_term();
        self.update_image_estimate();
        self.update_masked_image_estimate();
      end
    end

    function pre_allocate_loop_variables(self)
      self.imageEstimate = self.imageInput;
      self.maskedImageEstimate = self.imageEstimate;
      self.temporalUpdateTerm = zeros(size(self.imageEstimate), 'single');
    end

    function update_fidelity_term(self)
      fidelityTerm = self.imageInput - self.maskedImageEstimate;
      self.fidelityUpdateTerm = self.Weights.fidelity * fidelityTerm;
    end

    function update_temporal_term(self)
      % Take two differences in time dimension
      firstDiff = diff(self.imageEstimate, 1, 3);
      normalizationFactorSquared =  self.BETA_SQUARED + abs(firstDiff).^2;
      firstDiffNorm = firstDiff ./ sqrt(normalizationFactorSquared);
      secondDiff = diff(firstDiffNorm, 1, 3);

      % Fill term with pieces of differences
      self.temporalUpdateTerm(:,:,1) = firstDiffNorm(:,:,1);
      self.temporalUpdateTerm(:,:,2:end-1) = secondDiff;
      self.temporalUpdateTerm(:,:,end) = -firstDiffNorm(:,:,end);

      % Save as weighted temporal update term
      self.temporalUpdateTerm = self.Weights.temporal * self.temporalUpdateTerm;
    end

    function update_spatial_term(self)
      spatialTerm = TV_Spatial_2D(self.imageEstimate, self.BETA_SQUARED);
      self.spatialUpdateTerm = self.Weights.spatial * spatialTerm;
    end

    function update_image_estimate(self)
      imageUpdate = self.STEP_SIZE * (self.fidelityUpdateTerm + ...
                                      self.temporalUpdateTerm + ...
                                      self.spatialUpdateTerm);
      self.imageEstimate = self.imageEstimate + imageUpdate;
    end

    function update_masked_image_estimate(self)
      kSpaceEstimate = self.fftObject * self.imageEstimate;
      self.maskedImageEstimate = self.fftObject' * kSpaceEstimate;
    end

    function finalImage = unscale_image(self)
      finalImage = self.imageEstimate ./ self.scaleFactor;
    end
  end
end
%
% function accumulate_results(iIteration, Weights, fidelityUpdateTerm, imageEstimate)
%   % Fidelity Norm
%   fidelityNorm(iIteration)=sum(sum(sum(abs(fidelityUpdateTerm).^2)));
%   % Temporal Norm
%   temporalNorm(iIteration)=sum(sum(sum(abs(diff(imageEstimate,1,3)))));
%   % Spatial Norm
%   trunc=size(imageEstimate,1)-1;
%   sx_norm=abs(diff(imageEstimate,1,2));
%   sx_norm=sx_norm(1:trunc,1:trunc,:);
%   sy_norm=abs(diff(imageEstimate,1,1));
%   sy_norm=sy_norm(1:trunc,1:trunc,:);
%   spatialNorm(iIteration)=sum(sum(sum(sqrt(abs(sx_norm).^2+abs(sy_norm).^2))));
%
%   % Fill arrays
%   fidelity_terms = Weights.fidelity*fidelityNorm(1:iIteration);
%   temporal_terms = Weights.temporal*temporalNorm(1:iIteration);
%   spatial_terms = Weights.spatial*spatialNorm(1:iIteration);
%   totalNorm = fidelity_terms + temporal_terms + spatial_terms;
% end
%
% function plot_results(fidelity_terms, temporal_terms, spatial_terms, totalNorm)
%   figure(100);clf; hold on;
%   subplot(2,2,1); plot(fidelity_terms,'c*-'); title('Fidelity norm')
%   subplot(2,2,2); plot(temporal_terms,'kx-');  title('Temporal norm')
%   subplot(2,2,3); plot(spatial_terms,'bx-'); title('Spatial norm')
%   subplot(2,2,4); plot(totalNorm, 'bx-'); title('Total Cost')
%   %save('griddata_plots','fidelity_terms','temporal_terms','spatial_terms','totalNorm');
% end
