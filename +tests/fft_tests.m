function fft_tests(tester)
  % FIXME: This business of outputing number of iterations stinks. Should
  % probably change function to accept all parameters so that in testing they
  % *can* be specified.
  import Stcr.*
  % Test data comes from some gated cardiac perfusion data that has been PCA'd
  % the preInterpolator package was used to obtain suitable cartesian data and a mask.
  load('test_fft_data_4D.mat')

  % Create KSpaceData struct from test data
  KSpaceData.cartesianKSpace = testData4D;
  KSpaceData.cartesianMask = testKMask4D;
  KSpaceData.cartesianSize = [288, 288, 7, 3];;

  % Create Weights struct
  Weights.fidelity = 1;
  Weights.temporal = 0.1;
  Weights.spatial = 0.007;

  % Run first test
  load('fft_4D_recon_result.mat')
  warning = ['- Testing 4D fft recon, may take 10s of seconds. 70 iterations ' ...
             'must be used for this test to match the example data.'];
  disp(warning)

  presentResult = fft_recon_4D(KSpaceData, Weights);
  tester.test(officialResult, presentResult, 'Test stcr FFT recon 4D')

  % 3D data, keep only first time frame in data and trajectory to simulate *that*
  % kind of 3D.
  KSpaceData.cartesianKSpace = squeeze(testData4D(:,:,1,:));
  KSpaceData.cartesianMask = squeeze(testKMask4D(:,:,1,:));
  KSpaceData.cartesianSize = [288, 288, 3];

  load('fft_3D_recon_result.mat')
  disp('- Testing 3D fft recon, may take a few seconds. 70 iterations must be used.')

  presentResult = fft_recon_3D(KSpaceData, Weights);
  tester.test(officialResult, presentResult, 'Test stcr FFT recon 3D')

  % TODO - write test of kMask being smaller than testData
end
