function nufft_tests(tester)
  import Stcr.*
  % Test data comes from some gated cardiac perfusion data that has been PCA'd
  % The 4D version has a 3D trajectory and 2D density compensation matrix.
  load('test_nufft_data_4D.mat')

  % Put test data into KSpace struct
  KSpaceData.kSpace = testData4D;
  KSpaceData.trajectory = testTrajectory3D;
  KSpaceData.cartesianSize = [288, 288, 7, 3];

  % Weights Struct
  Weights.fidelity = 1;
  Weights.temporal = 0.1;
  Weights.spatial = 0.007;

  load('nufft_4D_recon_result.mat')
  disp('- Testing 4D nufft recon, may take 10s of seconds. 70 iterations must be used.')
  presentResult = nufft_recon_4D(KSpaceData, testDensityCompensation, Weights);

  tester.test(officialResult, presentResult, 'Test stcr NUFFT recon 4D')

  % 3D data, keep only first time frame in data and trajectory to simulate *that*
  % kind of 3D.
  KSpaceData.kSpace = squeeze(testData4D(:,:,1,:));
  KSpaceData.trajectory = squeeze(testTrajectory3D(:,:,1));
  KSpaceData.cartesianSize = [288, 288, 3];

  load('nufft_3D_recon_result.mat')

  disp('- Testing 3D nufft recon, may take a few seconds. 70 iterations must be used.')

  presentResult = nufft_recon_3D(KSpaceData, testDensityCompensation, Weights);

  tester.test(officialResult, presentResult, 'Test stcr NUFFT recon 3D')
end
