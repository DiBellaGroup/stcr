# nuFFT Recon

The 3D and 4D `nufft_recon` scripts provided by this package can be used to perform an stcr reconstruction of non-cartesian data provided the data, a trajectory, and density compensation are given. The input arguments are:

(KSpaceData, densityCompensation, Weights)

KSpaceData is a struct with the following fields

* kSpace - The non-Cartesian data.

* trajectory - A matrix corresponding to the kSpace matrix where each signal value in the `kSpace` matrix corresponds to a complex value in the `trajectory` matrix. The complex values range from -0.5 to 0.5 and are used in a way so that kx = real(complex_value) and ky = imag(complex_value).

* cartesianSize - An array giving the expected size of the cartesian multi-coil data. Because the trajectory is normalized between -0.5 and 0.5 the output image can be mapped to any matrix size.

The `densityCompensation` - Fessler's nuFFT method requires a matrix of densityCompensation values corresponding to the trajectory. Because it is a convolution-based method, it may map multiple intensities in non-Cartesian space to the same location in Cartesian space. Without a density compensation function this would erroneously double or triple the intensity there.

`Weights` - This struct must contain `fidelity` and `spatial` fields for single-time-frame data, and also must contain a `temporal` field for multi-time-frame data. These are the STCR reconstruction weights. For example:

```matlab
% Specify reconstruction weights.
Weights.temporal = 0.03;
Weights.spatial = 0.004;
Weights.fidelity = 1;
```

### Output

For "3D" data the output is a single reconstructed image, for "4D" data the output is a time-series of reconstructed images. The coils are combined assuming they are the last dimension of the data.
