# Reconstructor


The reconstructor class implements the STCR method as described in the paper referenced in the top level README. A sample `stcr.reconstruct` function is provided as a wrapper to the class for convenience and as an example of how to use the object.

<!-- TODO: Explain how STCR theory from the paper meets reality. STCR isn't my area of expertise so I'll leave this for someone else. -->
