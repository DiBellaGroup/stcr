# FFT Recon

The 3D and 4D `fft_recon` scripts provided by this package can be used to perform an STCR reconstruction of cartesian data provided the data and a mask are given. The input arguments are two structs: KSpaceData and Weights.

`KSpaceData` has the following fields

* cartesianKSpace - The cartesian data.

* cartesianMask - A logical mask corresponding to the data. The mask only needs to match dimension in the first two dimensions. If this program finds that the mask is smaller than the data it will use repmat to meet the difference.

* cartesianSize - An array giving the expected size of the data. This variable is especially needed for the nufft reconstructions, but here it is mostly used as a convenience for determining coil-number.

The `Weights` struct has the fields `fidelity` and `spatial` for single-time-frame data, and also must contain a `temporal` field for multi-time-frame data. These are the STCR reconstruction weights. For example:

```matlab
% Specify reconstruction weights.
Weights.temporal = 0.03;
Weights.spatial = 0.004;
Weights.fidelity = 1;
```

### Output

For "3D" data the output is a single reconstructed image, for "4D" data the output is a time-series of reconstructed images. The coils are combined via sum of squares assuming they are the last dimension of the data.
