function finalImage = reconstruct(kSpaceInput, fftObject, Weights)
  % A stub method for using the Reconstructor class as a function.
  reconstructor = Stcr.Reconstructor(kSpaceInput, fftObject, Weights);
  finalImage = reconstructor.reconstruct();
end
