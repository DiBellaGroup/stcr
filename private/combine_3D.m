function [ imageOutput ] = combine_3D( imageInput )
% Flatten a 3D chunk of image space by combining the coils via sum of squares
  % imageInput - dimensions [xHeight, yWidth, Ncoils]
  % imageOutput - dimensions [xHeight, yWidth]

  [xHeight, yWidth, nCoil] = size(imageInput);

  imageOutput = zeros(xHeight, yWidth);
  for iCoil = 1:nCoil
    coilSlice = imageInput(:,:,iCoil);
    squaredTerm = abs(coilSlice).^2;
    imageOutput = imageOutput + squaredTerm;
  end

  imageOutput = sqrt(imageOutput);
end
