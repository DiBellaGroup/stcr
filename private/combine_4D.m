function [ imageOutput ] = combine_4D( imageInput )
% Flatten a 4D chunk of image space by combining the coils via sum of squares
  % imageInput - dimensions [xHeight, yWidth, nTime, nCoil]
  % imageOutput - dimensions [xHeight, yWidth, nTime]

  [xHeight, yWidth, nTime, nCoil] = size(imageInput);

  imageOutput = zeros(xHeight, yWidth, nTime);
  for iCoil = 1:nCoil
    coilImage = imageInput(:,:,:,iCoil);
    squaredTerm = abs(coilImage).^2;
    imageOutput = imageOutput + squaredTerm;
  end

  imageOutput = sqrt(imageOutput);
end
